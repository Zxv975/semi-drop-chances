export function setup(ctx) {
    const id = 'drop-chances';
    const title = 'Show % Drop Chances';
    const desc =
        'This script injects into the drop chances menu and displays the % chance of receiving any item in the table. Also shows average GP values';


    const dropChances = () => {
        window.viewMonsterDrops = (monster, respectArea) => {
            if (monster === undefined) {
                return
            }

            if (monster.lootTable !== undefined) {
                let drops = '';
                let totalWeight = monster.lootTable.totalWeight
                if (monster.lootChance !== undefined) {
                    totalWeight = (totalWeight * 100) / monster.lootChance;
                }
                if (monster.lootChance > 0 && monster.lootTable.size > 0 && !(respectArea && game.combat.areaType === CombatAreaType.Dungeon)) {
                    drops = monster.lootTable.sortedDropsArray.map((drop) => {
                        let dropText = templateLangString('BANK_STRING', '40', {
                            qty: `${drop.maxQuantity}`,
                            itemImage: `<img class="skill-icon-xs mr-2" src="${drop.item.media}">`,
                            itemName: drop.item.name,
                        });
                        dropText += ` (${((drop.weight / totalWeight) * 100).toPrecision(3)}%)`;
                        return dropText;
                    }).join('<br>');
                }
                let bones = '';
                if (monster.bones !== undefined && !(respectArea && game.combat.selectedArea instanceof Dungeon && !game.combat.selectedArea.dropBones)) {
                    bones = `${getLangString('MISC_STRING', '7')}<br><small><img class="skill-icon-xs mr-2" src="${monster.bones.item.media}">${monster.bones.item.name}</small><br><br>`;
                } else {
                    bones = getLangString('COMBAT_MISC', '107') + '<br><br>';
                }
                let html = `<span class="text-dark">${bones}<br>`;
                if (drops !== '') {
                    html += `${getLangString('MISC_STRING', '8')}<br><small>${getLangString('MISC_STRING', '9')}</small><br>${drops}`;
                }
                html += '</span>';

                Swal.fire({
                    title: monster.name,
                    html: html,
                    imageUrl: monster.media,
                    imageWidth: 64,
                    imageHeight: 64,
                    imageAlt: monster.name,
                });
            }
        };

        window.viewItemContents = (item) => {
            if (!item) {
                item = game.bank.selectedBankItem.item;
            }
            const dropsOrdered = item.dropTable.sortedDropsArray;
            let totalWeight = item.dropTable.totalWeight;
            const drops = dropsOrdered.map((drop) => {
                let dropText = templateString(getLangString('BANK_STRING', '40'), {
                    qty: `${numberWithCommas(drop.maxQuantity)}`,
                    itemImage: `<img class="skill-icon-xs mr-2" src="${drop.item.media}">`,
                    itemName: drop.item.name,
                });
                dropText += ` (${((drop?.weight / totalWeight) * 100).toPrecision(3)}%)`;
                return dropText;
            }).join('<br>');

            Swal.fire({
                title: item.name,
                html: getLangString('BANK_STRING', '39') + '<br><small>' + drops,
                imageUrl: item.media,
                imageWidth: 64,
                imageHeight: 64,
                imageAlt: item.name,
            });
        };

        thievingMenu.showNPCDrops = (npc, area) => {
            const sortedTable = npc.lootTable.sortedDropsArray;
            const {minGP, maxGP} = game.thieving.getNPCGPRange(npc);
            let html = `<span class="text-dark"><small><img class="skill-icon-xs mr-2" src="${cdnMedia('assets/media/main/coins.svg')}"> ${templateLangString('MENU_TEXT', 'GP_AMOUNT', {gp: `${formatNumber(minGP)}-${formatNumber(maxGP)}`,})}</small><br>`;
            html += `${getLangString('THIEVING', 'POSSIBLE_COMMON')}<br><small>`;
            if (sortedTable.length) {
                html += `${getLangString('THIEVING', 'MOST_TO_LEAST_COMMON')}<br>`;
                const totalWeight = npc.lootTable.weight;
                html += sortedTable.map(({item, weight, minQuantity, maxQuantity}) => {
                    let text = `${maxQuantity > minQuantity ? `${minQuantity}-` : ''}${maxQuantity} x <img class="skill-icon-xs mr-2" src="${item.media}">${item.name}`;
                    text += ` (${((100 * weight) / totalWeight).toPrecision(3)}%)`;
                    return text;
                }).join('<br>');
                html += `<br>Average Value: ${npc.lootTable.averageDropValue.toPrecision(3)} GP<br>`;
            } else {
                html += getLangString('THIEVING', 'NO_COMMON_DROPS');
            }
            html += `</small><br>`;
            html += `${getLangString('THIEVING', 'POSSIBLE_RARE')}<br><small>`;
            const generalRareHTML = [];
            game.thieving.generalRareItems.forEach(({item, npcs, chance}) => {
                if (npcs === undefined || npcs.has(npc))
                    generalRareHTML.push(`${thievingMenu.formatSpecialDrop(item)} (${(game.thieving.areaUniqueChance * chance).toPrecision(3)}%)`);
            });
            html += generalRareHTML.join('<br>');
            html += `</small><br>`;
            if (area.uniqueDrops.length) {
                html += `${getLangString('THIEVING', 'POSSIBLE_AREA_UNIQUE')}<br><small>`;
                html += area.uniqueDrops.map((uniqueRareItem) => {
                    let chance = game.thieving.isPoolTierActive(3) ? 3 : 1;
                    let text = `${thievingMenu.formatSpecialDrop(uniqueRareItem.item, uniqueRareItem.quantity)} (${(game.thieving.areaUniqueChance * chance).toPrecision(3)}%)`
                    return text
                }).join('<br>');
                html += '</small><br>';
            }
            if (npc.uniqueDrop !== undefined) {
                html += `${getLangString('THIEVING', 'POSSIBLE_NPC_UNIQUE')}<br><small>${thievingMenu.formatSpecialDrop(
                    npc.uniqueDrop.item,
                    npc.uniqueDrop.quantity
                )} (${(game.thieving.getNPCPickpocket(npc)).toPrecision(3)}%)</small>`;
            }
            html += '</span>';

            Swal.fire({
                title: npc.name,
                html,
                imageUrl: npc.media,
                imageWidth: 64,
                imageHeight: 64,
                imageAlt: npc.name,
            });
        };

        mod.api.SEMI.log(id, 'Mod loaded successfully')
    };

    ctx.onInterfaceReady(() => {
        dropChances()
    })
}
